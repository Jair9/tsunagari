<?php require 'inc/header.php' ?>
<section class="Body">
  <h2 class="Body-title">Buy directly from Comiket</h2>

  <img src="img/logo-tsunagari.png" border="0" style="display: inline-block; width: 300px"/>
  <img src="img/price.png" border="0" style="display: inline-block; width: 300px"/>
  <img src="img/map.png" border="0" style="display: inline-block; width: 300px"/>
  <br />
  <p class="Body-text">Orders are shipped as soon as the event ends</p>
  <br />
  <img src="img/offer.png" border="0" style="display: inline-block; width: 300px"/>
  <img src="img/highest-priority.png" border="0" style="display: inline-block; width: 300px"/>
  <img src="img/offer2.png" border="0" style="display: inline-block; width: 300px"/>

  <div class="MainButtons">
    <div class="MainButton" id="btn-signup">
      <img class="MainButton-icon" alt="Signup" src="img/signup.svg">
      <span class="MainButton-label">Sign up</span>
    </div>
  </div>

  <div id="div-registration" class="Registration" ng-controller="ClientesController">
    <form ng-hide="nuevo.ready != undefined" name="nuevoCliente" method="post" ng-submit="nuevoListo() && setNuevo()" novalidate>
      <div class="Registration-block">
        <label for="div-registration-entry" class="Registration-label">Name</label>
        <input id="div-registration-entry" type="text" class="Registration-text" maxlength="255" ng-model="nuevo.nombre" required />
      </div>
      <div class="Registration-block">
        <label for="email" class="Registration-label">Email</label>
        <input id="email" type="text" class="Registration-text" maxlength="255" ng-model="nuevo.email" ng-blur="nuevo.email = nuevo.email.toLowerCase()" required />
      </div>
      <div class="Registration-block" style="margin-top: 12px;">
        <input id="privacy" type="checkbox" class="Registration-text" ng-model="nuevo.acepto" required />
        <label for="privacy" class="Registration-label" style="font-size: .8em;">I accept the <a href="privacy-policy.php" target="_blank">Privacy Policy</a></label>
      </div>
      <div class="Registration-block2">
        <input type="submit" class="Registration-button" value="Register" ng-disabled="!nuevoListo();">
      </div>
    </form>
    <span class="Registration-confirmation" ng-show="nuevo.ready == true">Registration Completed!</span>
    <span class="Registration-confirmation" ng-show="nuevo.ready == false">Thanks for returning! <br /> Your email is already registered. You will hear from us soon.</span>
  </div>

</section>
<?php require 'inc/footer.php' ?>

<?php require 'inc/header.php' ?>
 <section class="Body">
  <h1 class="Body-title">Privacy Policy</h1>
    <p class="Body-text">Thank you for visiting http://www.TsunagariStore.com.  As used herein, the terms “our,” “we,” “us,” and “Tsunagari” refer to both Tsunagari Mart and Tsunagari Store. The terms “you,” “your,” “customer,” and “buyer” refer to the person accessing this website.

Whether or not you are a registered member of Tsunagari your use of this platform gives your binding consent to our Privacy Policy. By accessing or using this website on any form of device, whether public or private, you signify that you have read, understand and agree to be bound by this Privacy Policy and any other applicable law. Tsunagari may change the Privacy Policy at any time without notice, effective upon posting it to Privacy Policy page. Your continued use of the Platform shall be considered your acceptance to the revised Privacy Policy. If you do not agree to Policies, please do not use this Platform. This Privacy Policy is incorporated into, and is bound to, the Terms and Conditions of Tsunagari.

We are committed to protecting and safeguarding your privacy. The purpose of this Privacy Policy is to inform you about the ways we may collect information from you through our site, as well as disclose any third-parties that the information could be shared to.


    Policies

A. Browsing the Site
You can browse Tsunagari without revealing any Personally Identifiable Information. As used herein, the term “Information” may be defined as, but is not limited to, your full name, date of birth, email address, physical address, telephone or cell phone number, location information through an IP address or GPS, Device identifier and hardware information, data about your purchases and other information about your movements in our site and when necessary for a purchase or order, credit card information. Information may be automatically received electronically or manually inputted by the customer. If you want to register or place an order you may voluntarily provide Personally Identifiable Information.

B. Registration for Tsunagari
When you register to shop online as a Customer we will collect Personally Identifiable Information. This Personally Identifiable Information is securely stored. You are able to select your own username and password; both are needed to enter the Site and order from our catalog. All usernames and passwords created with Tsunagari are the property of Tsunagari. You are responsible for any actions that take place while using any account that you login with. Keep your username and password secure and do not allow anyone else to use your username and password to access the Platform. Tsunagari is not responsible for any loss that results from unauthorized use of your username and password, with or without your knowledge. Any change of Personally Identifiable Information can be altered in your account settings.

C. Ordering from the Catalog
When you place an order through our catalog we will collect Personally Identifiable Information. We use this information to process your payment, deliver your order, and to give order status notifications. When you place an order thorough our catalogs your Personally Identifiable Information will be provided to our delivery and shipping partners.

D. Credit Card
Credit card information collected for cart checkout is used only to process payment. You may, however, voluntarily disclose credit card information to be stored and used for future cart checkouts. You may add, edit, and delete your stored card information.

E. Email
From time to time voluntarily provided Personally Identifiable Information given during registration by you will be used for Tsunagari to send out emails promoting contests, promotions, events, or surveys. We use this information to improve our team customer service and catalog items. Changes to email notification preferences are available and you are able to unsubscribe via your account settings.

F. Sharing of Personally Identifiable Information

Authorized employees within the company are on a need to know basis with Personal Identification Information collected from individual customers. We may disclose information to protect Tsunagari or Tsunagari’s registered customers against third-party claims. We may disclose information when required by law or a legitimate third-party request, legitimacy to be decided by Tsunagari. We may disclose information to any company partnering with Tsunagari in any way, to be used in anyway by the partnering company. We will not sell, trade, rent, or unnecessarily disclose your Personal Identifiable Information.

General

Use of Third-Party Media and Partnering Companies
Tsunagari may run third-party ads for Tsunagari’s partnering companies. In addition, we may use third-party media and companies to place ads for Tsunagari on our partnering company web sites. In addition, web links to third-party companies may be placed on the Tsunagari site for the buyer’s convenience or additional shopping options through our partners. Partnering company links may also be placed on Tsunagari to promote partnering contests, promotions, events, or surveys. The Tsunagari Privacy Policy will not extend to the third-party partnering company links or sites. We recommend reading through the partnering sites Privacy Policy, should you visit them through Tsunagari.

International Policies
Tsunagari’s hosting servers are located in the United States of America. Regardless of where you live, you consent to have your Personally Identifiable Information stored in the United States of America as well as transferred to anywhere Internationally between Tsunagari and our partnering companies in accordance with this Privacy Policy.

No Guarantee of Confidentiality
Tsunagari cannot guarantee 100% the security or confidentiality of the information you provide to us and will not be held liable for any unwelcome information sharing, however, Tsunagari will hold its confidentiality of your information in the highest regard.

Children’s Privacy Protection
We protect the privacy of children. Tsunagari does not target and is not intended for children under the age of 13, and we will not knowingly collect Personally Identifiable Information from them. Any data collected unknowingly from children under the age of 13 will be deleted from our system. You must be at least 18 years old or have parents’ permission to buy from us as a customer or buyer on our site.
</p>


</section>
<?php require 'inc/header.php' ?>

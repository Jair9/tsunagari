<?php
class Clientes {
	private $_db;

	public function __construct($db=NULL) {
		if(is_object($db))
        {
            $this->_db = $db;
        }
        else
        {
            $dsn = "mysql:host=".DB_HOST.";dbname=".DB_NAME;
            $this->_db = new PDO($dsn, DB_USER, DB_PASS);
        }
	}

  public function setNuevo($data) {
		$fecha_alta = date('Y-m-d H:i:s');
    $nombre = isset($data->nombre) ? $data->nombre:'';
		$email = isset($data->email) ? $data->email:'';
    $ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);

    if($this->existeEmail($email))
      return "{\"res\": false}";

  	$sql = "INSERT INTO
              clientes
            (fecha_alta, nombre, email, ip)
            VALUES
            (:fecha_alta, :nombre, :email, :ip)
  		";

    if($stmt = $this->_db->prepare($sql)) {
  		$stmt->bindParam(":fecha_alta", $fecha_alta);
      $stmt->bindParam(":nombre", $nombre);
      $stmt->bindParam(":email", $email);
      $stmt->bindParam(":ip", $ip);

  		if($stmt->execute()) {
  		  return "{\"res\": true}";
  		} else {
  			return "{\"res\": false}";
  		}
    } else {
      return "{\"res\": false}";
    }
	}

  public function existeEmail($email) {
		$sql = "SELECT
				      COUNT(*) AS existe
				    FROM
				      clientes
				    WHERE
				      email = :email";

		if($stmt = $this->_db->prepare($sql)) {
			$stmt->bindParam(":email", $email);
			$stmt->execute();
			if($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			  $existe = $row["existe"];
			  return $existe > 0;
			}
		}
	}


}

?>

<!DOCTYPE html>
<html lang="en" ng-app="tsunagari">
  <head>
    <title>tsunagari</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
  </head>
  <body>
    <header class="Header">
      <a href="index.php"><img class="Header-logo" alt="Tsunagari Logo" src="img/logosmall.png" /></a>
      <h1 class="Header-title">tsunagari shopping</h1>
    </header>

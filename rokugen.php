<?php require 'inc/header.php' ?>
<section class="Body">
  <h2 class="Body-title-gold">Fushigi no Kuni no Ototetsugaku Tasogare to Eien</h2>

  <img class="Body-2col" src="img/rokugen.png" border="0"/>
  <div class="Body-2col" style="padding: 0 16px; vertical-align: top;">
    <p class="Body-quote">
      &ldquo;He who does not walk along with others has inevitably to perish; everyone he encounters he collides with, and, as he is weak, he has necessarily to be crushed.&rdquo;
      <br />
      Marquis de Sade (France 1740-1814)
    </p>
    <p class="Body-text" >One concept generally associated with virtue is that of “Love”. Love for people, love for things, love exists in many forms.
    When we take love for other people, if that love is too weak or uncaring, it’s said one is cold.
    If that love is too strong and excessive, it’s said one is annoying.
    What form does the ideal love has? What’s the right amount of love people deem adequate?
    That may be nothing more than timely convenient words and attitude towards one’s object of love.
    That is to say, when one is looking for love, that love has to be a devoted love,
    while when one doesn’t need love, one doesn’t want to know anything about love.
    That is in truth a very convenient form of love, however, this is the kind of “love” most people seek.
    </p>
    <p class="Body-text">
    It is because people wish for everlasting love, that they want to make sure their love is not that kind of love.
    Thus, leading them to suspect about their partner.
    If something like true love really existed, then it would be something that only the person who loves could understand.
    Love described in words is superficial.</p>
  </div>

  <div class="MainButtons">
    <div class="MainButton" id="btn-signup">
      <img class="MainButton-icon" alt="Signup" src="img/signup.svg">
      <span class="MainButton-label">Pre Order</span>
    </div>
  </div>

  <div id="div-registration" class="Registration" ng-controller="ClientesController">
    <form ng-hide="nuevo.ready != undefined" name="nuevoCliente" method="post" ng-submit="nuevoListo() && setNuevo()" novalidate>
      <div class="Registration-block">
        <label for="div-registration-entry" class="Registration-label">Name</label>
        <input id="div-registration-entry" type="text" class="Registration-text" maxlength="255" ng-model="nuevo.nombre" required />
      </div>
      <div class="Registration-block">
        <label for="email" class="Registration-label">Email</label>
        <input id="email" type="text" class="Registration-text" maxlength="255" ng-model="nuevo.email" ng-blur="nuevo.email = nuevo.email.toLowerCase()" required />
      </div>
      <div class="Registration-block" style="margin-top: 12px;">
        <input id="privacy" type="checkbox" class="Registration-text" ng-model="nuevo.acepto" required />
        <label for="privacy" class="Registration-label" style="font-size: .8em;">I accept the <a href="privacy-policy.php" target="_blank">Privacy Policy</a></label>
      </div>
      <div class="Registration-block2">
        <input type="submit" class="Registration-button" value="Register" ng-disabled="!nuevoListo();">
      </div>
    </form>
    <span class="Registration-confirmation" ng-show="nuevo.ready == true">Registration Completed!</span>
    <span class="Registration-confirmation" ng-show="nuevo.ready == false">Thanks for returning! <br /> Your email is already registered. You will hear from us soon.</span>
  </div>

</section>
<?php require 'inc/footer.php' ?>

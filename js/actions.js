$(document).ready(function() {
  console.log("ready");
  $('#btn-signup').click(function() {
    $('#div-registration').show();
    $('#div-registration-entry').focus();
    $('html, body').animate({ scrollTop: $(document).height() }, 1000);
  });
});

(function() {

	var app = angular.module('tsunagari', []);

	/************
	SERVICES
	**************/

  app.service('ClientesService', ['$http', function($http) {

    var urlBase = 'req/clientes';
    var dataFactory = {};

    this.get = function () {
        return $http.get(urlBase);
    };

		this.getLogs = function () {
        return $http.get(urlBase + '/logs');
    };

    this.getID = function (id) {
        return $http.get(urlBase + '/' + id);
    };

		this.getSession = function () {
        return $http.get(urlBase + '/session');
    };

		this.login = function (obj) {
			console.log(cust);
      return $http.post(urlBase + "/login", cust);
    };

    this.setNuevo = function (obj) {
      return $http.post(urlBase + "/set-nuevo.php", obj);
    };

    this.update = function (obj) {
        return $http.put(urlBase + '/' + cust.ID, cust)
    };

    this.delete = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

  }]);

	/************
	CONTROLLERS
	**************/

  app.controller('ClientesController', [ '$http', '$scope', '$filter', 'ClientesService', function($http, $scope, $filter, ClientesService){

		$scope.nuevo = {};

		$scope.nuevoListo = function() {
			var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
			return $scope.nuevo.nombre != undefined && $scope.nuevo.nombre.length > 0 && $scope.nuevo.email != undefined && $scope.nuevo.email.length > 2 && emailPattern.test($scope.nuevo.email) && $scope.nuevo.acepto;
		}

		$scope.setNuevo = function() {
			ClientesService.setNuevo($scope.nuevo).success(function(data) {
				if(data.res) {
					$scope.nuevo.ready = true;
				}
				else {
					$scope.nuevo.ready = false;
				}
				$scope.show_new = false;
			});
		}

  }]);

	/************
	SOCKETS
	**************/






})();
